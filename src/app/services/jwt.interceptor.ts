import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class JwtInterceptor implements HttpInterceptor {

  private token: string;

  constructor() {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    if (this.token) {
      request = request.clone({ setHeaders: { Authorization: `Bearer ${this.token}` } });
    }
    return next.handle(request);
  }

  /**
   * Renseigner le token avec la valeur récupérée depuis l'authentification
   * @param token Token à setter
   */
  addToken(token: string): void {
    this.token = token;
  }

  /**
   * Reset du token
   */
  removeToken(): void {
    this.token = null;
  }
}
