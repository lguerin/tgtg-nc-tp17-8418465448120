import { Pipe, PipeTransform } from '@angular/core';
import { formatDistance, parseISO } from 'date-fns';
import { fr } from 'date-fns/locale';


@Pipe({
  name: 'expiredSince'
})
export class ExpiredSincePipe implements PipeTransform {

  transform(value: string): string {
    const from = parseISO(value);
    const to = new Date();
    return formatDistance(from, to, { addSuffix: true, locale: fr });
  }

}
