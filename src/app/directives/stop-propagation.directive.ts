import { Directive, HostListener } from '@angular/core';

@Directive({
  selector: '[ncStopPropagation]'
})
export class StopPropagationDirective {

  @HostListener('click', ['$event'])
  onClick(event: any): void {
    event.stopPropagation();
  }

}
