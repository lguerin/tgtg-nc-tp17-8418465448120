import { Routes } from '@angular/router';
import { StoresComponent } from './stores.component';
import { StoreComponent } from './store/store.component';
import { StoreResolver } from '../../resolvers/store.resolver';

export const STORES_ROUTES: Routes = [
  { path: '', component: StoresComponent },
  { path: ':id', component: StoreComponent, resolve: { store: StoreResolver } }
];
