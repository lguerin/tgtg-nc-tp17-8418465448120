import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { EMPTY, Observable } from 'rxjs';
import { StoreModel } from '../models/store.model';
import { TgtgService } from '../services/tgtg.service';
import { catchError } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class StoreResolver implements Resolve<StoreModel> {

  constructor(private tgtgService: TgtgService, private router: Router) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<StoreModel> {
    const storeId = route.paramMap.get('id');
    return this.tgtgService.getStore(storeId).pipe(catchError(err => this.handleError(err)));
  }

  private handleError(err: HttpErrorResponse): Observable<StoreModel> {
    switch (err.status) {
      case 400:
        console.warn('Les données envoyées au serveur ne sont pas correctes');
        break;
      case 401:
        this.redirectToPage('/login');
        break;
      case 403:
        this.redirectToPage('/login');
        break;
      default:
        console.error('Erreur inconnu: ', err);
        this.redirectToPage('/');
    }
    return EMPTY;
  }

  private redirectToPage(path: string): void {
    this.router.navigate([path]);
  }
}
